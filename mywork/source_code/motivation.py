# -*- coding: utf-8 -*-
import useful_function as uf
import pandas as pd

TOURS = 39
key_positions = {1, 2, 3, 4, 5, 6, 17, 18}

derbies = [['Arsenal', 'Tottenham', 'Chelsea', 'Fulham', 'West Ham'],
           ['Liverpool', 'Everton'],
           ['Man United', 'Man City'],
           ['Sunderland', 'Newcastle']]


# Check is derby
def is_derby(home, away):
    for derby in derbies:
        if home in derby and away in derby:
            return True
    return False


def get_current_tour(name, index_current, list):
    tour = 0
    for i in range(index_current):
        if list[i][1] == name or list[i][2] == name:
            tour += 1
    return tour


# get position of team
def get_position(name, tour, path_rank):
    data_rank = read_file_text(path_rank)
    for line in data_rank:
        if name == line[0] and tour == int(line[1]):
            return float(line[2])
    return None


# distance to the nearest “key position
def get_dist(position, score, score_4, score_17):
    if (position <= 6) or position == 17:
        return 0
    if position > 17:
        return score_17 - score
    return min(score_4 - score, score - score_17)


def read_file_text(path):
    df = pd.read_table(path, sep=',', names=('name', 'tour', 'pos'))
    data = []
    for i in range(len(df['pos'])):
        data.append([df['name'][i], df['tour'][i], df['pos'][i]])

    return data


# get score team
def get_score(i, list, name):
    score = 0
    for line in range(i - 1, -1, -1):
        if list[line][1] == name:
            # score += float(list[line][5])
            if float(list[line][5]) == 1:
                score += 3.0
            elif float(list[line][5]) == 0.5:
                score += 1.0
        elif list[line][2] == name:
            # score += 1 - float(list[line][5])
            if float(list[line][5]) == 0:
                score += 3.0
            elif float(list[line][5]) == 0.5:
                score += 1.0

    return score


list_file = ['0910.csv', '1011.csv', '1112.csv', '1213.csv', '1314.csv', '1415.csv', '1516.csv', '1617.csv',
             '1718.csv']
list_file_txt = ['0910.txt', '1011.txt', '1112.txt', '1213.txt', '1314.txt', '1415.txt', '1516.txt', '1617.txt',
                 '1718.txt']
path = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/'
path_rank = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/rank/'


# get tour, 1 if left < 6 and 0 otherwise;
def get_tour(left):
    if (left < 6):
        return 1
    return 0


def get_all_motivation():
    for k in range(len(list_file)):
        file_txt = path_rank + list_file_txt[k]
        data = uf.read_file_parse(path, list_file[k])
        i = 0
        score_4 = score_17 = 0
        data_motivation = [['name1','name2','motivation1','motivation2', 'diff_position']]
        print(file_txt)
        for line in data:
            name_home = line[1]
            name_away = line[2]
            result = line[5]
            score_home = get_score(i, data, name_home)
            score_away = get_score(i, data, name_away)
            current_match_home = get_current_tour(name_home, i + 1, data)
            current_match_away = get_current_tour(name_away, i + 1, data)

            left_home = TOURS - current_match_home
            left_away = TOURS - current_match_away

            position_home = get_position(name_home, current_match_home, file_txt)
            position_away = get_position(name_away, current_match_away, file_txt)
            diff_position = position_home - position_away

            if position_home == 6:
                score_4 = score_home
            if position_away == 6:
                score_4 = score_away
            if position_home == 17:
                score_17 = score_home
            if position_away == 17:
                score_17 = score_away
            dist_home = get_dist(position_home, score_home, score_4, score_17)
            dist_away = get_dist(position_away, score_away, score_4, score_17)
            tour_home = get_tour(left_home)
            tour_away = get_tour(left_away)

            derby = int(is_derby(name_home, name_away))
            param1_home = 1 - (dist_home / (3.0 * left_home))
            # param2_home = (tour_home  + dist_home)/2.0
            motivation_home = min(1, max(param1_home, derby))

            param1_away = 1 - (dist_away / (3.0 * left_away))
            # param2_away = (tour_away  + dist_away)/2.0
            motivation_away = min(1, max(param1_away, derby))
            data_motivation.append([name_home, name_away, motivation_home, motivation_away, diff_position])
            print(name_home, name_away, motivation_home, motivation_away)
            i += 1

        max_pos = get_max_diff_position(data_motivation)
        j = -1
        for line in data_motivation:
            # print(line)
            j += 1
            if j == 0:
                continue
            line[4] = 0.5 + float(line[4])/ (2.0 * max_pos)
            # print(goal, score)
        uf.write_file_csv(path + 'extracted_motivation_' + list_file[k], data_motivation)


# get max diff goal and score
def get_max_diff_position(data):
    max_position = 0
    i = -1
    for line in data:
        i += 1
        if i == 0:
            continue
        if max_position < float(line[4]):
            max_position = float(line[4])
    return max_position

get_all_motivation()
