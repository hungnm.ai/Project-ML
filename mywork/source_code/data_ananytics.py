import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


train = pd.read_csv('/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/source_code/extracted_full.csv')
y_train = np.array(train['result'])
x_train = train.drop(['result', 'name1', 'name2'], axis=1)

#: form_dif dependence
y_good = [form1 - form2 for form1, form2, res in zip(train['form1'], train['form2'], train['result']) if res == 1]
form_dif = train['form1'] - train['form2']
y_all = [1 / list(form_dif).count(form) for form in y_good]
plt.figure('Form hist')
plt.hist(y_good, weights=y_all, color='green')
plt.xlabel('Form difference')
plt.ylabel('Win rate')
plt.savefig('form_dependensce.png')


#: goal_dif dependence
# get all win match
matches_won = len([x for x in train['result'] if x == 1])
res = []
# [8, 4, 2, 10]=> (0, 8), (1, 4), (2, 2), (3, 10)
for cnt, row in enumerate(train.values):
    dif = row[8]
    print(row)
    # so tran thang ma co goal_diff < 7 /tong so tran thang
    res.append(sum([x / matches_won for x, y in zip(train['result'], train['goal_diff']) if x == 1 and y < dif]))
plt.figure('Goal diff plot')
plt.plot(train['goal_diff'], res, '.')
plt.xlabel('Goal diff')
plt.ylabel('Win rate')
plt.savefig('goal_dif_dependence.png')


#: score_dif dependence
res = []
for cnt, row in enumerate(train.values):
    dif = row[9]
    print(row)
    res.append(sum([x / matches_won for x, y in zip(train['result'], train['score_diff']) if x == 1 and y < dif]))
plt.figure('Score diff plot')
plt.plot(train['score_diff'], res, '.')
plt.xlabel('Score diff')
plt.ylabel('Win rate')
plt.savefig('score_dif_dependzence.png')

#: goal_dif dependence
matches_won = len([x for x in train['result'] if x == 1])
res = []
for cnt, row in enumerate(train.values):
    dif = row[12]
    res.append(sum([x / matches_won for x, y in zip(train['result'], train['diff_position']) if x == 1 and y < dif]))
plt.figure('Position diff plot')
plt.plot(train['diff_position'], res, '.')
plt.xlabel('Position diff')
plt.ylabel('Win rate')
plt.savefig('position_dif_dependence.png')



res = []
history = [0.0, 0.25, 0.5, 0.75, 1.0]
for cnt, row in enumerate(history):
    dif = row
    res.append(sum([x / matches_won for x, y in zip(train['result'], train['history']) if x == 1 and y == dif]) )
plt.figure('History plot')
plt.plot(history, res )
plt.xlabel('History')
plt.ylabel('Win rate')
plt.savefig('history_dependence.png')


res = []
form1 = []
for row in range(0, 11, 1):
    # if row in form1:
    #     continue
    form1.append(0.1*row)
    dif = 0.1 * row
    res.append(sum([x / matches_won for x, y in zip(train['result'], train['form1']) if x == 1 and y == dif]) )
plt.figure('Form plot')
plt.plot(form1, res)
plt.xlabel('Form')
plt.ylabel('Win rate')
plt.savefig('form1_dependence.png')
