# -*- coding: utf-8 -*-

import pandas as pd


list_file_form = ['0910.csv','1011.csv', '1112.csv', '1213.csv','1314.csv', '1415.csv', '1516.csv','1617.csv', '1718.csv']

for i in list_file_form:
    path = '/Users/manhhung/Documents/BKHN/ML/workspace/football-prediction/mywork/data/'
    form =  pd.read_csv(path + 'extracted_form_E0' + i)
    motivation =  pd.read_csv(path + 'extracted_motivation_' + i)
    goal_score_diff =  pd.read_csv(path + 'extracted_goal_score_diff_' + i)
    history =  pd.read_csv(path + 'extracted_history_' + i)
    df1 = history.merge(form, on=['name1', 'name2','result'])
    df2 = df1.merge(goal_score_diff,  on=['name1', 'name2'])
    df3 = df2.merge(motivation, on=['name1', 'name2'])
    df3.to_csv('extracted_' + i, index=False)
# print(df3)

extracted_0 = pd.read_csv('extracted_0910.csv')
extracted_1 = pd.read_csv('extracted_1011.csv')
extracted_2 = pd.read_csv('extracted_1112.csv')
extracted_3 = pd.read_csv('extracted_1213.csv')
extracted_4 = pd.read_csv('extracted_1314.csv')
extracted_5 = pd.read_csv('extracted_1415.csv')
extracted_6 = pd.read_csv('extracted_1516.csv')
extracted_7 = pd.read_csv('extracted_1617.csv')
extracted_8 = pd.read_csv('extracted_1718.csv')
full_extracted = pd.concat([ extracted_0,extracted_1, extracted_2, extracted_3, extracted_4, extracted_5, extracted_6, extracted_7, extracted_8])
full_extracted.to_csv('extracted_full.csv', index=False)

