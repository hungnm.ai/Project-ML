import csv

def convert_data(path, file):
    f = open(path + file, 'rb')
    reader = csv.reader(f)
    reader = list(reader)
    reader = reader[:]
    f.close()

    data = []
    i = 0
    for line in reader:
        result = 0
        print(line[6])
        if line[6] == 'H':
            result = 1
        elif line[6] == 'D':
            result = 0.5
        if i == 0:
            result = 'result'
        i += 1
        data.append([line[1], line[2], line[3], line[4], line[5], result])
        print(line[1] + ", " + line[2] + ", " + line[3] + ", " + line[4] + ", " + line[5] + ", " + str(result))
    write_file_csv(path + "parse_" + file, data)

# Thuc hien ghi data vao file csv
def write_file_csv(path_file, data=[]):
    myFile = open(path_file, 'w')
    with myFile:
        writer = csv.writer(myFile)
        writer.writerows(data)


# Ham doc file parse csv
def read_file_parse(path, file):
    file = path + "parse_E0" + file
    # print(file)
    f = open(file, 'rb')
    reader = csv.reader(f)
    reader = list(reader)
    reader = reader[1:]
    f.close()
    return reader



