import pandas as pd
import csv
from sklearn.neighbors import KNeighborsClassifier
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.model_selection import cross_val_score


def read_file_parse(file):
    ds = pd.read_csv(file, header=0)
    X = ds[["home", "away", "form1", "form2", "history", "goal_diff", "score_diff", "motivation1", "motivation2",
            "diff_position"]]
    Y = ds["result"]
    return X, Y


dataset = read_file_parse('extracted_full.csv')

# get values of X, Y
X = dataset[0].values
Y = dataset[1].values
print(type(X))
test_size = int(len(Y) * 0.2)
weight = [0.0, 0.0, 1.0, 1.0, 1.0, 3.0, 3.0, 1.0, 1.0, 3.0]
weight_array = np.array(weight)


# X_train, X_test, y_train, y_test = train_test_split(
#     X, Y, test_size=test_size)
# X_train = X_train * weight_array
# X_test = X_test * weight_array
#
# print("Training size: %d" % len(y_train))
# print("Test size    : %d" % len(y_test))


def knn():
    max_knn = 0
    for k in range(10, 30):
        accs = []
        clf = KNeighborsClassifier(n_neighbors=k, p=2, weights='distance')
        # clf.fit(X_train, y_train)
        # y_pred = clf.predict(X_test)
        accs.append(cross_val_score(clf, X, Y, cv=5))
        accuracy = 100 * np.sum(accs) / 5
        if accuracy > max_knn:
            max_knn = accuracy
        print("Accuracy of " + str(k) + " with NN: %.2f %%" % (accuracy))
    print("Max KNN: " + str(max_knn))
    return max_knn

def random_forest():
    arr = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 150, 160, 200, 250, 300, 350]
    max_rf = 0

    for i in range(len(arr)):
        accs = []
        clf = RandomForestClassifier(n_estimators=arr[i])
        accs.append(cross_val_score(clf, X, Y, cv=5))
        accuracy = 100 * np.sum(accs) / 5
        print("RF: " + str(accuracy))
        if accuracy > max_rf:
            max_rf = accuracy
    print("Max RF: " + str(max_rf))
    return max_rf



def svm():
    arrC = [1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 120.0, 130.0, 140.0, 150.0, 151.0,
            160.0, 170.0, 180.0, 190.0, 200.0, 250.0, 300.0, 320.0, 330.0, 340.0, 350.0]
    max_svm = 0

    for C0 in arrC:
        accs = []
        clf = SVC(C=C0)
        # clf.fit(X_train, y_train)
        # y_pred = clf.predict(X_test)
        accs.append(cross_val_score(clf, X, Y, cv=5))
        print(100 * np.sum(accs) / 5)
        accuracy = 100 * np.sum(accs) / 5
        if accuracy > max_svm:
            max_svm = accuracy

    print("Max SVM: " + str(max_svm))
    return max_svm




result = []
a = []
result.append(knn())
result.append(random_forest())
result.append(svm())
a.append(result)
with open("result.csv","a+") as my_csv:
    csvWriter = csv.writer(my_csv,delimiter=',')
    csvWriter.writerows(a)
